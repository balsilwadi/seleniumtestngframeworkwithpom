import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import pages.DepartPage;
import pages.HomePage;
import utilities.Driver;

public class Base {
    WebDriver driver;
    HomePage homePage;
    DepartPage departPage;

    @BeforeMethod
    public void setUp(){
        driver=Driver.getDriver();
        homePage = new HomePage(driver);
        departPage = new DepartPage(driver);
    }

    @AfterMethod
    public void tearDown(){
        Driver.quitDriver();
    }
}
