package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class HomePage {
    public HomePage(WebDriver driver){
        PageFactory.initElements(driver,this);
    }
    @FindBy(xpath = "//a[@class='app-components-GlobalHeader-globalHeader__expandedTabHeader--1Ra2H']")
    public List<WebElement> mainMenu;

    @FindBy(xpath = "//li[@role='tab']")
    public List<WebElement> bookTravelMenu;

    @FindBy(xpath = "(//input)[1]")
    public WebElement roundTrip;

    @FindBy(xpath = "(//label)[1]")
    public WebElement roundTripDisplay;

    @FindBy(xpath = "(//input)[2]")
    public WebElement oneWay;

    @FindBy(xpath = "(//label)[2]")
    public WebElement oneWayDisplay;

    @FindBy(xpath = "(//input)[3]")
    public WebElement bookWithMiles;

    @FindBy(xpath = "(//label)[3]")
    public WebElement bookWithMilesDisplay;

    @FindBy(xpath = "(//input)[4]")
    public WebElement flexibleDates;

    @FindBy(xpath = "(//label)[4]")
    public WebElement flexibleDatesDisplay;

    @FindBy(xpath = "(//input)[5]")
    public WebElement from;

    @FindBy(xpath = "(//input)[6]")
    public WebElement to;

    @FindBy(id = "DepartDate")
    public WebElement departDate;

    @FindBy(css = "button[aria-label='Move backward to switch to the previous month.']")
    public WebElement leftArrow;

    @FindBy(css = "td[aria-label='Thursday, January 20, 2022']")
    public WebElement _20;


    @FindBy(id = "passengerSelector")
    public WebElement travelers;

    @FindBy(xpath = "//button[@aria-label='Substract one Adult']")
    public WebElement addAdult;

    @FindBy(id = "cabinType")
    public WebElement businessOrFirstBox;

    @FindBy(id = "cabinType_item-2")
    public WebElement businessOrFirst;

    @FindBy(css = "button[aria-label='Find flights']")
    public WebElement findFlight;

    @FindBy(css = "button[class='app-components-QueryBuilder-Homepage-styles__autoCompleteItem--1WWvs']")
    public WebElement miami;





}


