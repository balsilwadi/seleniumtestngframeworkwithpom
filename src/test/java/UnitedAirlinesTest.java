import com.sun.source.tree.AssertTree;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.HomePage;
import utilities.ConfigReader;
import utilities.Waiter;

public class UnitedAirlinesTest extends Base{

    @Test(testName = "Main menu", priority = 1)
    public void validateMainMenu(){
        driver.get(ConfigReader.getProperties("URL"));

        for (int i = 0; i < homePage.mainMenu.size(); i++) {
            Assert.assertTrue(homePage.mainMenu.get(i).isDisplayed());
            Assert.assertEquals(homePage.mainMenu.get(i).getText(), ExpectedTexts.mainMenu[i]);
        }
    }

    @Test(testName = "Book travel menu", priority = 2)
    public void validateBookTravelMenu(){
        driver.get(ConfigReader.getProperties("URL"));

        for (int i = 0; i < ExpectedTexts.bookTravelMenu.length; i++) {
            Assert.assertTrue(homePage.bookTravelMenu.get(i).isDisplayed());
            Assert.assertEquals(homePage.bookTravelMenu.get(i).getText(), ExpectedTexts.bookTravelMenu[i]);
        }
    }

    @Test(testName = "Round-trip and One-way", priority = 3)
    public void validateRoundtripAndOneway(){
        driver.get(ConfigReader.getProperties("URL"));

        Assert.assertTrue(homePage.roundTripDisplay.isDisplayed());
        Assert.assertTrue(homePage.roundTrip.isEnabled());
        Assert.assertTrue(homePage.roundTrip.isSelected());

        Assert.assertTrue(homePage.oneWayDisplay.isDisplayed());
        Assert.assertTrue(homePage.oneWay.isEnabled());
        Assert.assertFalse(homePage.oneWay.isSelected());

        homePage.oneWay.click();
        Waiter.pause(10);
        Assert.assertTrue(homePage.oneWay.isSelected());
        Assert.assertFalse(homePage.roundTrip.isSelected());



    }

    @Test(testName = "Book with miles and Flexible dates", priority = 4)
    public void validateBookWithMilesFlexibleDates(){
        driver.get(ConfigReader.getProperties("URL"));

        Assert.assertTrue(homePage.bookWithMilesDisplay.isDisplayed());
        Assert.assertTrue(homePage.bookWithMiles.isEnabled());
        Assert.assertFalse(homePage.bookWithMiles.isSelected());

        Assert.assertTrue(homePage.flexibleDatesDisplay.isDisplayed());
        Assert.assertTrue(homePage.flexibleDates.isEnabled());
        Assert.assertFalse(homePage.flexibleDates.isSelected());

        homePage.flexibleDatesDisplay.click();
        homePage.bookWithMilesDisplay.click();
        Assert.assertTrue(homePage.bookWithMiles.isSelected());
        Assert.assertTrue(homePage.flexibleDates.isSelected());

        homePage.flexibleDatesDisplay.click();
        homePage.bookWithMilesDisplay.click();
        Assert.assertFalse(homePage.bookWithMiles.isSelected());
        Assert.assertFalse(homePage.flexibleDates.isSelected());


    }
    @Test(testName = "from Chicago, IL, US (ORD) to Miami, FL, US (MIA)", priority = 4)
    public void validateOnewayTicketSearch(){
        driver.get(ConfigReader.getProperties("URL"));

        homePage.oneWay.click();
        Waiter.pause(1);

        homePage.from.clear();
        Waiter.pause(1);

        homePage.from.sendKeys("Chicago, IL, US (ORD)");
        Waiter.pause(1);

        homePage.to.sendKeys("Miami, FL, US (MIA)");
        Waiter.pause(1);

        homePage.miami.click();
        Waiter.pause(1);

        homePage.departDate.click();
        Waiter.pause(1);

        homePage.leftArrow.click();
        Waiter.pause(1);

        homePage.leftArrow.click();
        Waiter.pause(1);

        homePage._20.click();
        Waiter.pause(1);

        homePage.travelers.click();
        Waiter.pause(1);


        homePage.addAdult.click();
        Waiter.pause(1);

        homePage.businessOrFirstBox.click();
        Waiter.pause(1);

        homePage.businessOrFirst.click();
        Waiter.pause(1);

        homePage.findFlight.click();
        Waiter.pause(2);

        Assert.assertEquals(departPage.depart.getText(),ExpectedTexts.depart);



    }

}
