public class ExpectedTexts {

    //testcase 1
    public static final String[] mainMenu = new String[]{
            "BOOK","MY TRIPS","TRAVEL INFO","MILEAGEPLUS® PROGRAM","DEALS","HELP"};

    //testcase 2
    public static final String[] bookTravelMenu = new String[]{
            "Book","Flight status","Check-in","My trips"};

    //depart
    public static final String depart = "Depart: Chicago, IL, US to Miami, FL, US";


}
